# Class: oidc
# ===========
#
# Class to configure a Tomcat and MitreID Connect image
#
# Examples
# --------
#
# @example
#
#    include 'oidc'
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2018 The Board of Trustees of the Leland Stanford Junior
# University
#
class oidc (
  $conf_dir,
  $war_file,
  $war_dir,
  $tomcat_user,
  $tomcat_uid,
  $tomcat_group,
  $tomcat_gid,
  $tomcat_home,
  $tomcat_confdir,
  $tomcat_wardir,
  $java_home,
  $tomcat_pkg,
  $java_pkg,
  $servlet_pkg,
) {

  user { $tomcat_user:
    ensure         => present,
    uid            => $tomcat_uid,
    gid            => $tomcat_gid,
    home           => $tomcat_home,
    managehome     => false,
    system         => true,
    shell          => '/bin/false',
    purge_ssh_keys => true,
    require        => Group[$tomcat_group],
  }

  group { $tomcat_group:
    ensure => present,
    gid    => $tomcat_gid,
    system => true,
  }

  include 'apt'

  Exec['apt_update'] -> Package<| |>

  Package[$java_pkg] -> Package[$tomcat_pkg]
  Package[$java_pkg] -> Package[$servlet_pkg]

  package { [
    $java_pkg,
    $tomcat_pkg,
    $servlet_pkg,
  ]:
    ensure  => latest,
    require => [
      Group[$tomcat_group],
      User[$tomcat_user]
    ],
  }

  #
  # App Install
  #

  file { $conf_dir:
    ensure  => directory,
    owner   => 'root',
    group   => $tomcat_group,
    mode    => '0755',
    require => Package[$tomcat_group],
  }

  file { "${tomcat_wardir}/oidc.war":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/${war_file}",
    require => Package[$tomcat_pkg],
  }

  #
  # Tomcat Configuration
  #

  file { "${tomcat_confdir}/server.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/tomcat/server.xml",
    require => Package[$tomcat_pkg],
  }

  file { "${tomcat_confdir}/web.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/tomcat/web.xml",
    require => Package[$tomcat_pkg],
  }

}
